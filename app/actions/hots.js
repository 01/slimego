/**
 * Created by lijizhuang on 2017/8/5.
 */
import { HOTS } from '../constants';
import Channel from '../channel';

const getHotKeywords = () => {
    return (dispatch, getStore) => {
        const channel = new Channel();
        return channel.getHotKeywords()
            .then(data => {
                var result = data.data;
                return dispatch({
                    type: HOTS.SYNCHOTS,
                    result
                })
            })
    }
};

export default {
    getHotKeywords
}