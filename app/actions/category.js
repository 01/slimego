/**
 * Created by lijizhuang on 2017/8/1.
 */
import { CATEGORY } from '../constants';

const syncKeyword = (data)=> {
    return (dispatch, getStore) => {
        dispatch({
            type: CATEGORY.SYNCKEYWORD,
            data
        });
    }
};

const switchTopTab = (data)=>{
    return (dispatch, getStore) => {
        dispatch({
            type: CATEGORY.SWITCHTOPTAB,
            data
        });
    }
};

export default {
    syncKeyword,
    switchTopTab
}