/**
 * Created by lijizhuang on 2017/7/7.
 */
import { HOME } from '../constants';
import Channel from '../channel';
/**
 * Get info of search result
 */
const getSearchResult = (keywords, page, size, type = 'all') => {
    return (dispatch, getStore) => {
        const channel = new Channel();
        return channel.getSearchResult(keywords, page, size, type)
            .then(data => {
                return dispatch({
                    type: HOME.SEARCH,
                    data
                })
            })
    }
};

const getBaiduSuggestion = (keyword) => {
    return (dispatch, getStore) => {
        const channel = new Channel();
        return channel.getBaiduSuggestion(keyword)
            .then(data=> {
                //console.log(data);
                return dispatch({
                    type: HOME.SUGGEST,
                    data
                })
            })
    }
};

const clearBaiduSuggestion = ()=> {
    var emptyArr = [];
    return (dispatch, getStore) => {
        dispatch({
            type: HOME.SUGGEST,
            emptyArr
        });
    }
};


export default {
    getSearchResult,
    getBaiduSuggestion,
    clearBaiduSuggestion
}