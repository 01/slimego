/**
 * Created by lijizhuang on 2017/7/23.
 */
'use strict'

// React
import React from 'react'
import {Image} from 'react-native'

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { NavigatorSearch } from './navigationConfiguration'

// Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
        navigationState: state.tabHome
    }
};

class SearchNavigation extends React.Component {
    static navigationOptions = ({navigation, screenProps}) => ({
        tabBarLabel: '首页',
        tabBarIcon: ({tintColor}) => (<Image source={require('../../images/icon_tabbar_homepage.png')} style={[{tintColor: tintColor},{width:25},{height:25}]}/>),
        tabBarVisible: navigation.state.params && navigation.state.params.hideTabBar  ? false : true
    });

    render(){
        const { navigationState, dispatch, navigation } = this.props;
        const {screenProps} = this.props;
        return (
            <NavigatorSearch
                navigation={
                  addNavigationHelpers({
                    dispatch: dispatch,
                    state: navigationState
                  })
                }
                screenProps={screenProps}
            />
        )
    }
}

export default connect(mapStateToProps)(SearchNavigation)
