/**
 * Created by lijizhuang on 2017/7/6.
 */
//搜索区域
import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Keyboard,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    NativeModules,
} from 'react-native';

var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');
const dismissKeyboard = require('dismissKeyboard');

import SuggestionListView from './SuggestionListView';
import SearchResults from './SearchResults'

var Platform = require('Platform');
import SplashScreen from 'react-native-splash-screen';
import {NavigationActions} from 'react-navigation';
var UMNative = (Platform.OS === 'ios' ? NativeModules.UMNative : NativeModules.UmengNativeModule);

export default class SearchIndex extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedKeyword: '',
            showSuggest: false,
            isDisable: false
        };

    }

    componentDidMount() {
        const {homeActions} = this.props.screenProps;
        //if(Platform.OS === 'android')
        //    SplashScreen.hide(); //解决安卓加载offline] bundle出现白屏的情况
    }

    componentWillReceiveProps(props) {
        const {home} = this.props.screenProps;
    }

    render() {
        const {home, homeActions} = this.props.screenProps;

        return (
            <TouchableWithoutFeedback onPress={()=>{dismissKeyboard(); this.setState({showSuggest: false})}}>
                <View style={styles.container}>
                    <Image source={{uri: 'app_bg'}} style={styles.backgroundImage}/>
                    <View style={styles.searchArea}>
                        <Image source={{uri: 'b_logo'}} style={styles.blogo}/>
                        {/*搜索框和按钮*/}
                        <View style={{flexDirection:'row'}}>
                            <TextInput ref="searchTB"
                                       returnKeyType="search"
                                       placeholder="搜索关键词，如：iOS视频教程"
                                       style={styles.searchInput}
                                       underlineColorAndroid='transparent'
                                       onSubmitEditing={this.doSearch.bind(this)}
                                       onChangeText={this.getValue.bind(this)}
                                       value={this.state.selectedKeyword}
                            />
                            <TouchableHighlight disabled={this.state.isDisable} style={styles.searchBtn} onPress={this.doSearch.bind(this)}
                                                underlayColor="white">
                                <Image
                                    style={{width:24, height:24}}
                                    source={{uri: 'search_btn'}}/>
                            </TouchableHighlight>
                        </View>
                        {/*搜索关键词提示框*/}
                        { this.state.showSuggest &&
                        <View style={styles.suggestionView}>
                            <SuggestionListView onSelectKeyword={this.selectKeyword.bind(this)}
                                                style={styles.suggestionListView} home={home} actions={homeActions}/>
                        </View>
                        }
                    </View>
                    <View style={styles.otherArea}>
                        <Text style={styles.announce}>免责声明：本站仅提供百度网盘的资源搜索服务，目的在于方便用户查阅学习资料，不存放任何文件和内容，如有侵权，请联系百度网盘. </Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }

    selectKeyword(keyword) {
        //const {navigator} = this.props;

        this.setState({
            selectedKeyword: keyword,
            showSuggest: false
        });

        //跳转至搜索结果页
        //const {homeActions} = this.props.screenProps;
        //homeActions.sendKeyword(keyword);
        //this.props.navigation.navigate('SearchResults', { keyword: keyword });

        this.refs.searchTB.focus();
    }

    getValue(text) {
        const {homeActions} = this.props.screenProps;
        var value = text;
        if (value.trim().length > 0) {
            homeActions.getBaiduSuggestion(value);
        }
        else {
            homeActions.clearBaiduSuggestion();
        }

        this.setState({
            showSuggest: true,
            selectedKeyword: text
        });
    }

    doSearch(event) {
        //const {navigator} = this.props;

        //防止重复提交
        this.setState({
            isDisable:true
        });
        Keyboard.dismiss();
        //var input = this.refs.searchTB;
        //var inputValue = this.state.selectedKeyword;
        var skeyword = this.state.selectedKeyword.trim();
        if (skeyword.length > 0) {
            const {cateActions} = this.props.screenProps;
            var keyword = this.state.selectedKeyword;
            cateActions.syncKeyword(keyword);
            this.props.navigation.navigate('SearchResults', {keyword: keyword});
            UMNative.onEventWithParameters("100001",{keyword: keyword});

            //隐藏tabbar
            const setParamsAction = NavigationActions.setParams({
                params: { hideTabBar: true },
                key: 'SearchNavigation'
            });
            this.props.navigation.dispatch(setParamsAction);
        }

        this.timer = setTimeout(async()=>{
            await this.setState({isDisable:false})//2秒后可点击
        },2000)
    }

}

SearchIndex.propTypes = {
    actions: PropTypes.object
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center'
    },

    backgroundImage: {
        //resizeMode: 'cover', // or 'stretch'
        top: 0,
        left: 0,
        width: width,
        height: height,
        resizeMode: 'cover',
        position: 'absolute'
    },

    searchArea: {
        position: 'relative',
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: height <= 480 ? 20 : (0.15 * height),  //iphone4-> 20 iphone5->40 iphone6->60
        zIndex: 999
    },

    blogo: {
        width: 155,
        height: 155
    },

    searchInput: {
        width: 0.85 * width,
        height: 43,
        backgroundColor: 'white',
        marginTop: 2,

        // 设置圆角
        borderRadius: 5,

        paddingLeft: 5,
        // 内左边距
        paddingRight: 28,
        borderColor: '#227db6',
        borderWidth: 1,
    },

    searchBtn: {
        position: 'absolute',
        right: 8,
        top: 10
    },

    suggestionView: {
        width: 0.85 * width,
        alignItems: 'center',
        zIndex: 999
    },

    suggestionListView: {
        position: 'absolute',
        width: 0.85 * width,
    },

    otherArea: {
        flex: 1,
        //解决安卓的bug（子视图是absolute，父视图有大小限制，则超出部分会被其他视图截断，需将其他干扰的视图设置成absolute）
        position: 'absolute',
        zIndex: 1
    },

    announce:{
        textAlign:'center',
        fontSize: 10,
        marginTop: 130,
        width: width - 60,
        marginLeft: 30,
        backgroundColor: 'transparent',
        lineHeight:15
    }
});