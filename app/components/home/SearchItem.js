/**
 * Created by lijizhuang on 2017/7/28.
 */
import React, {PureComponent} from "react";
import {StyleSheet, View, Alert, TouchableOpacity, Image, TouchableHighlight, NativeModules} from "react-native";
import {Button, ListItem, Left, Right, Body, Thumbnail, Text, Icon} from "native-base";

var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');
var Platform = require('Platform');
var UMNative = (Platform.OS === 'ios' ? NativeModules.UMNative : NativeModules.UmengNativeModule);

export default class SearchItem extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const rowID = this.props.index;
        const rowData = this.props.item;
        var date = rowData.uploadTime.length > 0 ? rowData.uploadTime.split(" ") : '--';
        return (
            <View>
                {rowID === 0 && <Text style={styles.total}>共{rowData.total}个结果</Text>}
                <View style={styles.outerView}>
                    <TouchableHighlight underlayColor='#F0FFFF' onPress={this.jumpDetail.bind(this, rowData)}>
                        <View style={styles.innerView}>
                            {/*左*/}
                            <Image source={require('../../images/prefix.png')}
                                   style={styles.prefix}/>
                            {/*中*/}
                            <View style={styles.content}>
                                {/*文件名*/}
                                <Text numberOfLines={1} style={styles.name}>{rowData.name}</Text>
                                {/*文件信息*/}
                                <View style={styles.info}>
                                    <Text
                                        style={styles.infoItem}>类型: {rowData.isFolder === true ? '文件夹' : rowData.suffix}</Text>
                                    <Text
                                        style={styles.infoItem}>大小: {rowData.fileSize}</Text>
                                    <Text numberOfLines={1}
                                          style={styles.infoItem}>时间: {date[0]}</Text>
                                </View>
                            </View>

                            {/*右*/}
                            {this.getResourceIcon(rowData.suffix)}
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    jumpDetail(rowData) {
        //跳转，此处做判断是避免react-navigation的bug，相同的路由信息会在不同的tab item会重复跳转
        if (this.props.navigation.state.routeName === "SearchResults")
            this.props.navigation.navigate('SearchItemDetail', {rowData: rowData});
        else
            this.props.navigation.navigate('SearchItemDetailFromHots', {rowData: rowData});

        UMNative.onEventWithParameters("100002",{resource: rowData.name});
    }

    getResourceIcon(suffix) {
        //压缩包
        if (suffix === "rar") {
            return <Image source={require('../../images/rar.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "7z") {
            return <Image source={require('../../images/7z.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "zip") {
            return <Image source={require('../../images/zip.png')}
                          style={styles.suffix}/>
        }

        //视频格式
        if (suffix === "avi" || suffix === "wmv" || suffix === "rmvb"
            || suffix === "rm" || suffix === "flash" || suffix === "mp4"
            || suffix === "mid" || suffix === "3gp" || suffix === "mkv"
            || suffix === "mov") {
            return <Image source={require('../../images/video.png')}
                          style={styles.suffix}/>
        }

        //图片格式
        if (suffix === "jpg" || suffix === "jpeg" || suffix === "png"
            || suffix === "tiff" || suffix === "psd" || suffix === "gif"
            || suffix === "bmp" || suffix === "svg") {
            return <Image source={require('../../images/image.png')}
                          style={styles.suffix}/>
        }

        //音乐格式
        if (suffix === "mp3" || suffix === "wma" || suffix === "wav"
            || suffix === "mod" || suffix === "cd" || suffix === "ogg"
            || suffix === "rm" || suffix === "mp3pro" || suffix === "flac"
            || suffix === "aac") {
            return <Image source={require('../../images/music.png')}
                          style={styles.suffix}/>
        }


        //文档格式
        if (suffix === "pdf") {
            return <Image source={require('../../images/pdf.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "txt") {
            return <Image source={require('../../images/txt.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "doc" || suffix === "docx") {
            return <Image source={require('../../images/doc.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "xls" || suffix === "xlsx") {
            return <Image source={require('../../images/xls.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "ppt" || suffix === "pptx") {
            return <Image source={require('../../images/ppt.png')}
                          style={styles.suffix}/>
        }

        //应用格式
        if (suffix === "ipa") {
            return <Image source={require('../../images/ipa.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "apk") {
            return <Image source={require('../../images/apk.png')}
                          style={styles.suffix}/>
        }
        if (suffix === "exe") {
            return <Image source={require('../../images/exe.png')}
                          style={styles.suffix}/>
        }

        return <Image source={require('../../images/other.png')}
                      style={styles.suffix}/>
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    total: {
        marginTop: 20,
        marginBottom: 10,
        marginLeft: 0.05 * width,
        fontSize: 15,
        color: 'grey'
    },
    outerView: {
        width: 0.9 * width,
        marginLeft: 0.05 * width,
        borderBottomWidth: 0.5,
        borderBottomColor: '#4d4d4d'
    },
    innerView: {
        flexDirection: 'row',
        paddingTop: 7,
        paddingBottom: 7,
    },
    prefix: {
        width: 0.05 * width,
        height: 0.05 * width,
        marginRight: 8,
        marginTop: 3
    },
    content: {
        flexDirection: 'column',
        width: 0.77 * width
    },
    name: {
        fontSize: 16
    },
    info: {
        flexDirection: 'row',
        marginTop: 6
    },
    infoItem: {
        color: 'grey',
        fontSize: 13,
        marginRight: 11
    },
    suffix: {
        width: 0.05 * width,
        height: 0.05 * width,
        //marginRight: 8,
        marginTop: 3
    },


});