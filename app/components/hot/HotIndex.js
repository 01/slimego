/**
 * Created by lijizhuang on 2017/7/6.
 */
import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Image,
    Text,
    View,
    ScrollView,
    TouchableHighlight,
    ActivityIndicator,
    InteractionManager,
    NativeModules
} from 'react-native';

import {NavigationActions} from 'react-navigation';

var Platform = require('Platform');
var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');
var sections = [];
var UMNative = (Platform.OS === 'ios' ? NativeModules.UMNative : NativeModules.UmengNativeModule);

export default class HotIndex extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        const {hotsActions} = this.props.screenProps;
        hotsActions.getHotKeywords();
    }

    componentWillReceiveProps(props) {
        InteractionManager.runAfterInteractions(()=> {
            const {hots, hotsActions} = props.screenProps;
            if (Object.keys(hots.items).length > 0) {
                this.setState({
                    loading: false
                });
            }
        });
    }

    render() {
        const {hots, hotsActions} = this.props.screenProps;
        if (sections.length == 0) {
            for (var obj in hots.items) {
                var section;
                var title = obj;
                var rows = [];
                for (var j = 0; j < hots.items[obj].length; j++) {
                    var subItem = hots.items[obj][j];
                    rows.push(<TouchableHighlight style={styles.itemButton} underlayColor={'#1C9DD7'} key={subItem}
                                                  onPress={this.doSearch.bind(this, subItem)}><Text numberOfLines={1}
                                                                                                    style={styles.itemText}>{subItem}</Text></TouchableHighlight>);
                }
                sections.push(<View style={styles.sectionsView} key={title}><View
                    style={{flexDirection: 'row', marginBottom: 5}}><Image source={require('../../images/hot.png')}
                                                                           style={styles.hotsIcon}/><Text
                    style={styles.itemTitle}>{title}</Text></View><View style={styles.items}>{rows}</View></View>);
            }
        }
        //for (var i = 0; hots && i < hots.items; i++) {
        //    var section;
        //    var title = hots.items[i].title;
        //    var rows = [];
        //    for (var j=0; j < hots.items[i].values; j++) {
        //        rows.push(<Text>{hots.items[i].values[j]}</Text>);
        //    }
        //    sections.push(<View><Text>{title}</Text>{rows}</View>);
        //}

        return (
            <View>
                {
                    <ScrollView style={styles.scrollView} bounces={ false }>
                        <Image source={{uri: 'app_bg'}} style={styles.backgroundImage}/>
                        { this.state.loading &&
                        <View style={styles.loadingView}>
                            <ActivityIndicator color={'dimgray'} size={'small'}/>
                            <Text
                                style={[{fontSize: 16}, {marginLeft: 5}]}>{'努力加载中...'}</Text>
                        </View>
                        }
                        {sections}
                    </ScrollView>
                }
            </View>
        )
    }

    doSearch(keyword) {
        const {cateActions} = this.props.screenProps;
        cateActions.syncKeyword(keyword);

        this.props.navigation.navigate('SearchResultsFromHots', {keyword: keyword});
        UMNative.onEventWithParameters("100001",{keyword: keyword});

        //隐藏tabbar
        const setParamsAction = NavigationActions.setParams({
            params: { hideTabBar: true },
            key: 'HotNavigation',
        });
        this.props.navigation.dispatch(setParamsAction);
    }
}

HotIndex.propTypes = {
    actions: PropTypes.object
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    backgroundImage: {
        //resizeMode: 'cover', // or 'stretch'
        top: 0,
        left: 0,
        width: width,
        height: height * 2,
        resizeMode: 'cover',
        position: 'absolute'
    },
    loadingView: {
        position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor:'white',
        width:width,
        marginTop: 30,
        backgroundColor: 'transparent'
    },
    scrollView: {
        height: height - 49,
        width: width,
        backgroundColor: '#fff'
    },
    sectionsView: {
        width: width,
        marginTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        marginBottom: 40
    },
    items: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    hotsIcon: {
        width: 18,
        height: 18
    },
    itemTitle: {
        backgroundColor: 'transparent',
        fontSize: 16
    },
    itemButton: {
        width: width / 2 - 20
    },
    itemText: {
        fontSize: 14,
        textAlign: 'center',
        height: 30,
        paddingTop: 7,
        color: '#4d4d4d'
    }
});